package com.android.assistant.utils;


public interface Const {
    String MENU_TAG = "menu";
    String MAIN_TAG = "main";
    String PHONE_INPUT_TAG = "phone_board";
    String PHONE_TAG = "phone";
    String CONTACTS_TAG = "contact";
    String MESAGE_TAG = "message";
    String CAMERA_TAG = "camera";
    String RADIO_TAG = "radio";
    String FACEBOOK_TAG = "facebook";
    String ALARM_TAG = "alarm";
    String GAME_TAG = "game";
    String MAP_TAG = "map";
    String SETTING_TAG = "setting";
    String CALL_TAG = "call";
    String UPDATE_CONTACTS_TAG = "new contacts";
    String NEW_MESSAGE_TAG = "new message";
    String COMMING_TAG = "comming";

    int REQ_CODE_SPEECH_INPUT = 100;
    String[] TAG_LIST = new String[]{
            PHONE_TAG,
            CONTACTS_TAG,
            MESAGE_TAG,
            CAMERA_TAG,
            RADIO_TAG,
            FACEBOOK_TAG,
            ALARM_TAG,
            GAME_TAG,
            MAP_TAG,
            SETTING_TAG,
            MENU_TAG,
            MAIN_TAG
    };

    String[] HomeCommands = new String[] {
            SETTING_TAG, //
            MENU_TAG, //
            MAP_TAG,
            ALARM_TAG,
            GAME_TAG,
            FACEBOOK_TAG,
            CAMERA_TAG,
            RADIO_TAG,
            CONTACTS_TAG, //
            MESAGE_TAG, //
            PHONE_TAG   //
    };


    String[] MenuCommands = new String[] {
            SETTING_TAG, //
            MAP_TAG,
            ALARM_TAG,
            GAME_TAG,
            FACEBOOK_TAG,
            CAMERA_TAG,
            RADIO_TAG,
            CONTACTS_TAG, //
            MESAGE_TAG, //
            PHONE_TAG   //
    };


    String[] PhoneCommands = new String[] {
            CALL_TAG //
    };

}
