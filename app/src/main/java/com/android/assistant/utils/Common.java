package com.android.assistant.utils;


import android.content.Context;
import android.os.Build;
import android.text.InputType;
import android.widget.EditText;

import com.android.assistant.view.MainActivity;

import java.util.ArrayList;

public class Common {
    public static void removeKeyboard(EditText editText){
        if (Build.VERSION.SDK_INT >= 11) {
            editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            editText.setTextIsSelectable(true);
        } else {
            editText.setRawInputType(InputType.TYPE_NULL);
            editText.setFocusable(true);
        }
    }

    public static void openMic(Context context) {
        ((MainActivity) context).startSpeechToText();
    }

    public static void openVoice(Context context, String message, boolean isStartMic) {
        ((MainActivity) context).startTextToSpeech(message, isStartMic);
    }

    public static void switchToScreen(Context context, String tag) {
        ((MainActivity) context).switchFragmentByTag(tag, true);
    }

    public static void goBack(Context context) {
        ((MainActivity) context).onBackPressed();
    }
    public static String tryToConvertNumber(String text){
        String result = "";
        String[] array = text.split(" ");
        for(String element: array) {
            switch (element.toLowerCase()) {
                case "one":
                    result += "1";
                    break;
                case "oh":
                    result += "0";
                    break;
                case "zero":
                    result += "0";
                    break;
                case "two":
                    result += "2";
                    break;
                case "three":
                    result += "3";
                    break;
                case "four":
                    result += "14";
                    break;
                case "five":
                    result += "5";
                    break;
                case "six":
                    result += "6";
                    break;
                case "seven":
                    result += "7";
                    break;
                case "eight":
                    result += "8";
                    break;
                case "nine":
                    result += "9";
                    break;
                case "0":
                    result += "0";
                    break;
                case "1":
                    result += "1";
                    break;
                case "2":
                    result += "2";
                    break;
                case "3":
                    result += "3";
                    break;
                case "4":
                    result += "4";
                    break;
                case "5":
                    result += "5";
                    break;
                case "6":
                    result += "6";
                    break;
                case "7":
                    result += "7";
                    break;
                case "8":
                    result += "8";
                    break;
                case "9":
                    result += "9";
                    break;
                default: return "";
            }
        }
        return result;
    }

    public static MainActivity getMainActivity(Context context){
        return (MainActivity) context;
    }


    public static ArrayList<String> getContacts() {

        ArrayList<String> mData = new ArrayList<>();
        mData.add("Cô giang: 0123456789");
        mData.add("Mr obama: 0484516318");
        mData.add("Mr clipton: 095416549");
        mData.add("Gấu dog: 021455656");
        mData.add("Em gái nuôi: 0165548464");
        mData.add("Bạn hello: 0484516319");
        mData.add("Bố xinh gái: 0548465");
        mData.add("Mẹ đẹp trai: 05848965");
        mData.add("Ông hàng xóm: 087965864");
        mData.add("Thợ ống nước: 03546849");
        mData.add("Mr puttin: 09879886");
        mData.add("Mr trump: 844641654");
        mData.add("Mr hci: 84854648");

        return mData;
    }
}
