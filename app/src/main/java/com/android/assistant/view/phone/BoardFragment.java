package com.android.assistant.view.phone;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.assistant.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTextChanged;

import static com.android.assistant.utils.Common.removeKeyboard;

public class BoardFragment extends Fragment {
    private View mParentView;
    @BindView(R.id.number_input) EditText mEditext;
    @BindView(R.id.back_space)ImageButton mBackButton;
    @BindView(R.id.suggest_text)TextView mSuggestText;
    @BindView(R.id.table_layout)TableLayout mTableLayout;

    private PhoneInputFragment phoneInputFragment;

    public static BoardFragment newInstance(PhoneInputFragment phoneInputFragment) {
        BoardFragment boardFragment = new BoardFragment();
        boardFragment.phoneInputFragment = phoneInputFragment;
        return boardFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_board, container, false);
            ButterKnife.bind(this, mParentView);
            removeKeyboard(mEditext);
            onKeyboardPressListener();
        }

        return mParentView;
    }
    public void onKeyboardPressListener(){
        for(int i = 0; i < mTableLayout.getChildCount(); i++) {
            TableRow tableRow = (TableRow) mTableLayout.getChildAt(i);
            for(int j = 0; j < tableRow.getChildCount(); j++) {
                final Button button = (Button) tableRow.getChildAt(j);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        insertText(button.getText().toString());
                    }
                });
            }
        }
    }
    public void insertText(String character) {
        String text = mEditext.getText().toString();
        if(!mEditext.isFocused()) mEditext.setSelection(text.length());
        int cusorPosition = mEditext.getSelectionEnd();
        String first = text.substring(0, cusorPosition);
        String second = text.substring(cusorPosition);
        mEditext.setText(first+character+second);
        mEditext.setSelection(cusorPosition+1);
    }
    @OnClick(R.id.back_space)
    public void onDeleteClick(View v) {
        String text = mEditext.getText().toString();
        if(!mEditext.isFocused()) mEditext.setSelection(text.length());
        int cusorPosition = mEditext.getSelectionEnd();
        if(cusorPosition != 0)  {
            String first = text.substring(0, cusorPosition-1);
            String second = text.substring(cusorPosition);
            mEditext.setText(first+second);
            mEditext.setSelection(cusorPosition-1);
        }
    }
    @OnLongClick(R.id.back_space)
    public boolean onDeleteLongClick(View v) {
        mEditext.setText("");
        mSuggestText.setText("");
        return true;
    }
    @OnClick(R.id.suggest_text)
    public void onSuggestTextClick(View v) {
        if(mSuggestText.getText().equals("Find in contacts!")) {
            phoneInputFragment.onFindInContacts(mEditext.getText().toString());
        }
    }
    @OnClick(R.id.call_button)
    public void onCallClick(View v) {
        if(mSuggestText.getText().toString().isEmpty() && mEditext.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "The number cant be empty!", Toast.LENGTH_SHORT).show();

        } else {
            if(phoneInputFragment != null) {
                phoneInputFragment.onCall(mSuggestText.getText().equals("Find in contacts!")? "" : mSuggestText.getText().toString(),
                        mEditext.getText().toString());
            }
        }
    }
    public void onTextChange() {
        String text = mEditext.getText().toString();
        String suggest = "Cô giang";
        if(text.length() >= 3 && suggest.contains("0123456789")) {
            mSuggestText.setText(suggest);
            mSuggestText.setTypeface(null, Typeface.NORMAL);
            mSuggestText.setTextColor(getContext().getResources().getColor(R.color.selected));
        } else if(text.isEmpty()) {
            mSuggestText.setText("");
        } else {
            mSuggestText.setText("Find in contacts!");
            mSuggestText.setTypeface(null, Typeface.ITALIC);
            mSuggestText.setTextColor(getContext().getResources().getColor(R.color.un_selected));
        }
        if (mEditext.getText().toString().isEmpty()) mBackButton.setVisibility(View.GONE);
        else mBackButton.setVisibility(View.VISIBLE);
    }
    @OnTextChanged(R.id.number_input)
    protected void onTextChanged(CharSequence text) {
        String featureName = text.toString();
        onTextChange();
    }

    public void onSetData(String name, String number) {
        mEditext.setText(number);
        if(name.isEmpty()) {
            mSuggestText.setText("Find in contacts!");
            mSuggestText.setTypeface(null, Typeface.ITALIC);
            mSuggestText.setTextColor(getContext().getResources().getColor(R.color.un_selected));
        }
        else {
            mSuggestText.setText(name);
            mSuggestText.setTypeface(null, Typeface.NORMAL);
            mSuggestText.setTextColor(getContext().getResources().getColor(R.color.selected));
        }
    }
}
