package com.android.assistant.view.message;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.assistant.R;
import com.android.assistant.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

import static com.android.assistant.utils.Common.goBack;
import static com.android.assistant.utils.Common.openVoice;
import static com.android.assistant.utils.Common.tryToConvertNumber;

public class NewMesageFragment extends Fragment  {


    public static String WELLCOME = "Input  " +
            "Number or Name to set receiver. Say " +
            "Send to finish input message and send.";

    private View mParentView;
    private String name, number;
    @BindView(R.id.name)
    EditText mName;
    @BindView(R.id.message)
    EditText mMessage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //openVoice(getContext(), WELLCOME, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_message_send, container, false);
            ButterKnife.bind(this, mParentView);
            if(name!= null && !name.isEmpty()) mName.setText(name);
            else if(number != null && !number.isEmpty()) mName.setText(number);
        }

        return mParentView;
    }

    public static NewMesageFragment newInstance(String name, String number) {
        NewMesageFragment updateContactsFragment = new NewMesageFragment();
        updateContactsFragment.name = name;
        updateContactsFragment.number = number;
        return updateContactsFragment;
    }
    @OnLongClick(R.id.send_button)
    public boolean onSaveLongClick(View v) {
        ((MainActivity) getActivity()).startSpeechToText();
        return true;
    }
    @OnClick(R.id.send_button)
    public void onSaveClick(View v) {
        if(!mName.getText().toString().isEmpty() && !mMessage.getText().toString().isEmpty()){
            Toast.makeText(getContext(), "Sending message!", Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
        } else {
            Toast.makeText(getContext(), "Name or message must be filled!", Toast.LENGTH_SHORT).show();
        }

    }
    public void setName(String text) {
        mName.setText(text);
    }
    public void setMessage(String text) {
        mMessage.setText(text);
    }


    public void onRecognize(String text) {
        if(text.equals("back")){
            openVoice(getContext(), "OK. Go back!", true);
            goBack(getContext());
        } else if(text.equals("back to home")) {
            openVoice(getContext(), "OK. Go back to home!", true);
            goBack(getContext());
        } else if(text.equals("stop")) {
            openVoice(getContext(), "OK. Bye. See you again!", false);
        }
        else {
            String[] array = text.split("\\s+");
            if (text.contains("send")) {
                if(!mName.getText().toString().isEmpty()&& !mMessage.getText().toString().isEmpty()) {
                    openVoice(getContext(), "OK, Sending message success", true);
                } else {
                    openVoice(getContext(), "Sorry. receiver or message. must be filled !", true);
                }
            } else {
                if (text.contains("number")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("number")) {
                            isAdd = true;
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty() && !tryToConvertNumber(t).isEmpty()) {
                        openVoice(getContext(), " OK. Set phone number. " + tryToConvertNumber(t), true);
                        setName(tryToConvertNumber(t));
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the number you say!", true);
                    }
                } else if (text.contains("name")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("name")) {
                            isAdd = true;
                            t = "";
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty()) {
                        openVoice(getContext(), " OK. Set name. " + t, true);
                        setName(t);
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the name you say!", true);
                    }
                } else if (text.contains("message")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("message")) {
                            isAdd = true;
                            t = "";
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty()) {
                        openVoice(getContext(), " OK. Set message. " + t, true);
                        setMessage(t);
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the message you say!", true);
                    }
                } else {
                    openVoice(getContext(), "Sorry. I dont understand!", true);
                }
            }
        }
    }
}
