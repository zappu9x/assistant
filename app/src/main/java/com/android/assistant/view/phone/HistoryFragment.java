package com.android.assistant.view.phone;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.android.assistant.R;
import com.android.assistant.view.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.mrapp.android.bottomsheet.BottomSheet;

public class HistoryFragment extends Fragment implements HistoryRecyclerAdapter.OnItemClickCallBack {
    private ArrayList<String> mData = new ArrayList<>();
    private HistoryRecyclerAdapter mHistoryRecyclerAdapter;
    private View mParentView;
    @BindView(R.id.recycle_view) RecyclerView mRecyclerView;
    @BindView(R.id.empty_layout) LinearLayout mEmptyLayout;
    @BindView(R.id.clear_button) FloatingActionButton mClearButton;

    private PhoneInputFragment phoneInputFragment;

    public static HistoryFragment newInstance(PhoneInputFragment phoneInputFragment) {
        HistoryFragment historyFragment = new HistoryFragment();
        historyFragment.phoneInputFragment = phoneInputFragment;
        return historyFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData.add("Cô giang: 0123456789");
        mData.add("Mr obama: 0484516318");
        mData.add("0214556475");
        mData.add("Mr clipton: 095416549");
        mData.add("Gấu chó: 021455656");
        mData.add("Em gái nuôi: 0165548464");
        mData.add("0987158435");
        mData.add("0214556475");
        mData.add("Mr obama: 0484516318");
        mData.add("0214556475");
        mData.add("Mr clipton: 095416549");
        mData.add("Gấu chó: 021455656");
        mData.add("Em gái nuôi: 0165548464");
        mData.add("Mr obama: 0484516318");
        mData.add("0214556475");
        mData.add("Mr clipton: 095416549");
        mData.add("Gấu chó: 021455656");
        mData.add("Em gái nuôi: 0165548464");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_history, container, false);
            ButterKnife.bind(this, mParentView);
            mEmptyLayout.setVisibility(View.GONE);
            mHistoryRecyclerAdapter = new HistoryRecyclerAdapter(getContext(), mData, this);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mHistoryRecyclerAdapter);
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if(dy > 5 && mClearButton.isShown()){
                        mClearButton.hide();
                    }
                    if (dy <-5&& !mClearButton.isShown()){
                        mClearButton.show();
                    }
                }
            });
        }

        return mParentView;
    }

    @OnClick(R.id.clear_button)
    public void setOnClearHistory(View v) {
        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(getContext());
        mAlertDialog.setMessage("Are you want to delete all item!")
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }})
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mData.clear();
                        mHistoryRecyclerAdapter.notifyDataSetChanged();
                        mEmptyLayout.setVisibility(View.VISIBLE);
                        mClearButton.hide();
                    }
                })
                .show();
    }

    @Override
    public void onMenuItemClicked(String name, String number) {
        if(phoneInputFragment != null) {
            phoneInputFragment.onCall(name, number);
        }
    }

    @Override
    public void onMessageClicked(String name, String number) {
        ((MainActivity)getContext()).onNewMessage(name, number);
    }

    @Override
    public void onMenuItemLongClicked(final int position, final String name, final String number) {
        BottomSheet.Builder builder = new BottomSheet.Builder(getContext());
        builder.setTitleColor(getResources().getColor(R.color.colorPrimary));
        if(name.isEmpty()) {
            builder.setTitle(number);
            builder.addItem(0, R.string.edit_call, R.drawable.ic_phone_black);
            builder.addItem(1, R.string.add_contacts, R.drawable.ic_add_contacts_black);
        } else {
            builder.setTitle(name);
            builder.addItem(0, R.string.edit_call, R.drawable.ic_phone_black);
            builder.addItem(2, R.string.edit_contacts, R.drawable.ic_edit_black);
        }
        builder.addItem(3, R.string.delete_history, R.drawable.ic_delete_black);
        builder.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int p, long id) {
                switch ((int) id) {
                    case 0:
                        if(phoneInputFragment != null) {
                            phoneInputFragment.onSetToBoard(name, number);
                        }
                        break;
                    case 1:
                        ((MainActivity) getActivity()).onUpdateContacts("", number);
                        break;
                    case 2:
                        ((MainActivity) getActivity()).onUpdateContacts(name, number);
                        break;
                    case 3:
                        onDeleteItem(position);
                        break;
                }
            }
        });
        BottomSheet bottomSheet = builder.create();
        bottomSheet.show();
    }

    public void onDeleteItem(int position) {
        mData.remove(position);
        mHistoryRecyclerAdapter.notifyItemRemoved(position);
        if(mHistoryRecyclerAdapter.getItemCount() == 0){
            if(mClearButton.isShown()) {
                mClearButton.hide();
            }
            mEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

}
