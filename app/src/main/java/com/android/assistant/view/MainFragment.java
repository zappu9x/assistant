package com.android.assistant.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.assistant.R;
import com.android.assistant.utils.Const;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

import static com.android.assistant.utils.Common.openMic;
import static com.android.assistant.utils.Common.openVoice;
import static com.android.assistant.utils.Common.switchToScreen;

public class MainFragment extends Fragment {
    public static String WELLCOME = "Say your function you want use!. Say menu to get menu. Say phone to get phone. ...etc..";
    private View mParentView;

    public static MainFragment newInstance() {
        MainFragment mainFragment = new MainFragment();
        return mainFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openVoice(getContext(), WELLCOME, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_main, container, false);
            ButterKnife.bind(this, mParentView);
        }
        return mParentView;
    }

    public void onRecognize(String text) {
        boolean isSuccess = false;
        for(String tag: Const.HomeCommands) {
            if(text.equals(tag)) {
                switchToScreen(getContext(), tag);
                isSuccess = true;
                break;
            }
        }
        if(!isSuccess) {
            if(text.equals("stop")) {
                openVoice(getContext(), "OK. Bye. See you again!", false);
            }
            else {
                openVoice(getContext(), "Sorry. I dont understand!", true);
            }
        }
    }

    @OnClick({R.id.start_button, R.id.home_button, R.id.call_button, R.id.message_button})
    public void onHandleButtonClick(View v) {
        switch (v.getId()) {
            case R.id.start_button:
                openMic(getContext());
                break;
            case R.id.home_button:
                ((MainActivity)getContext()).switchFragmentByTag(Const.MENU_TAG, false);
                break;
            case R.id.call_button:
                ((MainActivity)getContext()).switchFragmentByTag(Const.PHONE_TAG, false);
                break;
            case R.id.message_button:
                ((MainActivity)getContext()).switchFragmentByTag(Const.MESAGE_TAG, false);
                break;
        }
    }

    @OnLongClick(R.id.home_button)
    public boolean receiveCall(View v){
        switchToScreen(getContext(), Const.COMMING_TAG);
        return true;
    }

}
