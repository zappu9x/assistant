package com.android.assistant.view.phone;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;



public class PhonePagerAdapter extends FragmentPagerAdapter  {


    public PhoneInputFragment mPhoneInputFragment;
    private Fragment[] mFragments;
    public PhonePagerAdapter(FragmentManager fm, PhoneInputFragment mPhoneInputFragment) {
        super(fm);
        this.mPhoneInputFragment = mPhoneInputFragment;
        mFragments = new Fragment[] {
                HistoryFragment.newInstance(mPhoneInputFragment),
                BoardFragment.newInstance(mPhoneInputFragment),
                ContactsFragment.newInstance(mPhoneInputFragment) };
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments[position];
    }

    @Override
    public int getCount() {
        return 3;
    }

    public void onSetToBoard(String name, String number) {
        BoardFragment boardFragment = (BoardFragment) mFragments[1];
        boardFragment.onSetData(name, number);
    }

    public void onFindInContacts(String number) {
        ContactsFragment contactsFragment = (ContactsFragment) mFragments[2];
        contactsFragment.onFindKeyword(number);
    }
}
