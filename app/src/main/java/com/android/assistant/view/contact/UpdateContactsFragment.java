package com.android.assistant.view.contact;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.assistant.R;
import com.android.assistant.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

import static com.android.assistant.utils.Common.goBack;
import static com.android.assistant.utils.Common.openVoice;
import static com.android.assistant.utils.Common.tryToConvertNumber;

public class UpdateContactsFragment extends Fragment  {


    public static String WELLCOME = "Say  " +
            "Number one two three four five. To set number. one two three four five . " +
            "Name hello. To set name hello. " +
            "Address Ha Noi Viet Nam. To set address. ha noi viet Nam. " +
            "Email obama. to set email obama. ";

    private View mParentView;
    private String name, number;
    @BindView(R.id.name)
    EditText mName;

    @BindView(R.id.number)
    EditText mNumber;
    @BindView(R.id.address)
    EditText mAddress;
    @BindView(R.id.mail)
    EditText mMail;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //openVoice(getContext(), WELLCOME, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_contacts_update, container, false);
            ButterKnife.bind(this, mParentView);
            mName.setText(name);
            mNumber.setText(number);
        }

        return mParentView;
    }

    public static UpdateContactsFragment newInstance(String name, String number) {
        UpdateContactsFragment updateContactsFragment = new UpdateContactsFragment();
        updateContactsFragment.name = name;
        updateContactsFragment.number = number;
        return updateContactsFragment;
    }
    @OnLongClick(R.id.saver_button)
    public boolean onSaveLongClick(View v) {
        ((MainActivity) getActivity()).startSpeechToText();
        return true;
    }
    @OnClick(R.id.saver_button)
    public void onSaveClick(View v) {
        getActivity().onBackPressed();
    }
    public void setName(String text) {
        mName.setText(text);
    }
    public void setMail(String text) {
        mMail.setText(text);
    }
    public void setAddress(String text) {
        mAddress.setText(text);
    }
    public void setPhone(String text) {
        mNumber.setText(text);
    }


    public void onRecognize(String text) {
        if(text.equals("back")){
            openVoice(getContext(), "OK. Go back!", true);
            goBack(getContext());
        } else if(text.equals("back to home")) {
            openVoice(getContext(), "OK. Go back to home!", true);
            goBack(getContext());
        } else if(text.equals("stop")) {
            openVoice(getContext(), "OK. Bye. See you again!", false);
        }
        else {
            String[] array = text.split("\\s+");
            if (text.equals("finish")) {
                if(!mName.getText().toString().isEmpty()&& !mNumber.getText().toString().isEmpty()) {
                    openVoice(getContext(), "OK, Update success", true);
                } else {
                    openVoice(getContext(), "Sorry. Name. or. number. must be filled !", true);
                }
            } else {
                if (text.contains("number")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("number")) {
                            isAdd = true;
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty() && !tryToConvertNumber(t).isEmpty()) {
                        openVoice(getContext(), " OK. Set phone number. " + tryToConvertNumber(t), true);
                        setPhone(tryToConvertNumber(t));
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the number you say!", true);
                    }
                } else if (text.contains("name")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("name")) {
                            isAdd = true;
                            t = "";
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty()) {
                        openVoice(getContext(), " OK. Set name. " + t, true);
                        setName(t);
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the name you say!", true);
                    }
                } else if (text.contains("address")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("address")) {
                            isAdd = true;
                            t = "";
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty()) {
                        openVoice(getContext(), " OK. Set address. " + t, true);
                        setAddress(t);
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the address you say!", true);
                    }
                }else if (text.contains("email")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("email")) {
                            isAdd = true;
                            t = "";
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty()) {
                        openVoice(getContext(), " OK. Set email. " + t, true);
                        setMail(t);
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the email you say!", true);
                    }
                } else {
                    openVoice(getContext(), "Sorry. I dont understand!", true);
                }
            }
        }
    }
}
