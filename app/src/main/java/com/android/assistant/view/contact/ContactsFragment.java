package com.android.assistant.view.contact;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;

import com.android.assistant.R;
import com.android.assistant.utils.Const;
import com.android.assistant.view.MainActivity;
import com.android.assistant.view.phone.ContactsRecyclerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import de.mrapp.android.bottomsheet.BottomSheet;

import static com.android.assistant.utils.Common.getContacts;
import static com.android.assistant.utils.Common.goBack;
import static com.android.assistant.utils.Common.openVoice;
import static com.android.assistant.utils.Common.switchToScreen;
import static com.android.assistant.utils.Common.tryToConvertNumber;

public class ContactsFragment extends Fragment implements ContactsRecyclerAdapter.OnItemClickCallBack {

    public static String WELLCOME = "Input " +
            "Number, Name To find contacts. " +
            "New Contacts. to create new contacts";
    private View mParentView;
    private ArrayList<String> mData = new ArrayList<>();
    private ArrayList<String> mDataCopy = new ArrayList<>();
    private ArrayList<String> mDataQuery = new ArrayList<>();
    private ArrayList<String> mDataChange = new ArrayList<>();
    private ContactsRecyclerAdapter mContactsRecyclerAdapter;

    @BindView(R.id.keyword_input) EditText mKeyword;
    @BindView(R.id.add_button)
    FloatingActionButton mAddButton;
    @BindView(R.id.recycle_view) RecyclerView mRecyclerView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataCopy = getContacts();
        Collections.sort(mData, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2) > 0 ? 1 : o1.compareTo(o2) < 0 ? -1 : 0;
            }
        });
        mData.addAll(mDataCopy);
       // openVoice(getContext(), WELLCOME, true);
    }

    public void onRecognize(String text) {
        if(text.equals("back")){
            openVoice(getContext(), "OK. Go back!", true);
            goBack(getContext());
        } else if(text.equals("back to home")) {
            openVoice(getContext(), "OK. Go back to home!", true);
            goBack(getContext());
        } else if(text.equals("stop")) {
            openVoice(getContext(), "OK. Bye. See you again!", false);
        }
        else {
            String[] array = text.split("\\s+");
            if (text.equals("new contacts")) {
                switchToScreen(getContext(), Const.UPDATE_CONTACTS_TAG);
            } else if (text.contains("number")) {
                String t = "";
                boolean isAdd = false;
                for (String s : array) {
                    if (s.equals("number")) {
                        isAdd = true;
                    } else if (isAdd) {
                        t += s + " ";
                    }
                }
                if (!t.isEmpty() && !tryToConvertNumber(t).isEmpty()) {
                    openVoice(getContext(), " OK. Set phone number. " + tryToConvertNumber(t), true);
                    setKeyword(tryToConvertNumber(t));
                } else {
                    openVoice(getContext(), " Sorry!. cant understant the number you say!", true);
                }
            } else if (text.contains("name")) {
                String t = "";
                boolean isAdd = false;
                for (String s : array) {
                    if (s.equals("name")) {
                        isAdd = true;
                        t = "";
                    } else if (isAdd) {
                        t += s + " ";
                    }
                }
                if (!t.isEmpty()) {
                    openVoice(getContext(), " OK. Set name. " + t, true);
                    setKeyword(t);
                } else {
                    openVoice(getContext(), " Sorry!. cant understant the name you say!", true);
                }
            } else {
                openVoice(getContext(), "Sorry. I dont understand!", true);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_contacts, container, false);
            ButterKnife.bind(this, mParentView);
            mContactsRecyclerAdapter = new ContactsRecyclerAdapter(getContext(), mData, this);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mContactsRecyclerAdapter);
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if(dy > 5 && mAddButton.isShown()){
                        mAddButton.hide();
                    }
                    if (dy <-5&& !mAddButton.isShown()){
                        mAddButton.show();
                    }
                }
            });
        }

        return mParentView;
    }

    @OnTextChanged(R.id.keyword_input)
    protected void onTextChanged(CharSequence text) {
        String featureName = text.toString();
        onTextChange();
    }

    public void onTextChange() {
        mDataQuery.clear();
        String text = mKeyword.getText().toString().toLowerCase();
        for(String dataCopy: mDataCopy) {
            if(dataCopy.toLowerCase().contains(text)) {
                mDataQuery.add(dataCopy);
            }
        }
        mDataChange.clear();
        mDataChange.addAll(mData);
        mDataChange.removeAll(mDataQuery);
        for(String del: mDataChange){
            for(int i = 0; i < mData.size(); i++) {
                if(mData.get(i).equals(del)) {
                    mData.remove(i);
                    mContactsRecyclerAdapter.notifyItemRemoved(i);
                }
            }
        }
        mDataChange.clear();
        mDataChange.addAll(mDataQuery);
        mDataChange.removeAll(mData);
        for(String insert: mDataChange){
            boolean isInsert = false;
            for(int i = 0; i < mData.size(); i++) {
                if(mData.get(i).compareTo(insert) >= 0) {
                    mData.add(i, insert);
                    mContactsRecyclerAdapter.notifyItemInserted(i);
                    isInsert = true;
                    break;
                }
            }
            if(!isInsert) {
                mData.add(mData.size(), insert);
                mContactsRecyclerAdapter.notifyItemInserted(mData.size());
            }
        }

    }
    @OnClick(R.id.mic_button)
    public void onStartAsistant(View v) {
        ((MainActivity) getActivity()).startSpeechToText();
    }

    @OnClick(R.id.add_button)
    public void onNewContacts(View v) {
        ((MainActivity) getActivity()).onUpdateContacts("", "");
    }
    public void onFindKeyword(String number) {
        mKeyword.setText(number);
    }

    @Override
    public void onMenuItemClicked(String name, String number) {
        ((MainActivity) getActivity()).onUpdateContacts(name, number);
    }

    @Override
    public void onMenuItemLongClicked(final int position, final String name, final String number) {
        BottomSheet.Builder builder = new BottomSheet.Builder(getContext());
        builder.setTitleColor(getResources().getColor(R.color.colorPrimary));
        builder.setTitle(name);
        builder.addItem(1, R.string.edit, R.drawable.ic_edit_black);
        builder.addItem(2, R.string.delete, R.drawable.ic_delete_black);
        builder.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int p, long id) {
                switch ((int) id) {
                    case 1:
                        ((MainActivity) getActivity()).onUpdateContacts(name, number);
                        break;
                    case 2:
                        onDeleteItem(position);
                        break;
                }
            }
        });
        BottomSheet bottomSheet = builder.create();
        bottomSheet.show();
    }

    @Override
    public void onCallClicked(String name, String number) {
        ((MainActivity) getActivity()).onGoCallScreen(name, number);
    }

    @Override
    public void onMessageClicked(String name, String number) {
        ((MainActivity)getContext()).onNewMessage(name, number);
    }

    public void onDeleteItem(int position) {
        String del = mData.remove(position);
        mDataCopy.remove(del);

        mContactsRecyclerAdapter.notifyItemRemoved(position);
    }
    public void setKeyword(String keyword) {
        mKeyword.setText(keyword);
    }
}
