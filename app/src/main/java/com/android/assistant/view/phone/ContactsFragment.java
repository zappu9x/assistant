package com.android.assistant.view.phone;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;

import com.android.assistant.R;
import com.android.assistant.utils.Common;
import com.android.assistant.view.MainActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import de.mrapp.android.bottomsheet.BottomSheet;

public class ContactsFragment extends Fragment implements ContactsRecyclerAdapter.OnItemClickCallBack {
    private View mParentView;
    private ArrayList<String> mData = new ArrayList<>();
    private ArrayList<String> mDataCopy = new ArrayList<>();
    private ArrayList<String> mDataQuery = new ArrayList<>();
    private ArrayList<String> mDataChange = new ArrayList<>();
    private ContactsRecyclerAdapter mContactsRecyclerAdapter;

    private PhoneInputFragment phoneInputFragment;
    @BindView(R.id.keyword_input) EditText mKeyword;
    @BindView(R.id.recycle_view) RecyclerView mRecyclerView;

    public static ContactsFragment newInstance(PhoneInputFragment phoneInputFragment) {
        ContactsFragment contactsFragment = new ContactsFragment();
        contactsFragment.phoneInputFragment = phoneInputFragment;
        return contactsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataCopy = Common.getContacts();
        Collections.sort(mData, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2) > 0 ? 1 : o1.compareTo(o2) < 0 ? -1 : 0;
            }
        });
        mData.addAll(mDataCopy);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_contacts_child, container, false);
            ButterKnife.bind(this, mParentView);
            mContactsRecyclerAdapter = new ContactsRecyclerAdapter(getContext(), mData, this);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mRecyclerView.setAdapter(mContactsRecyclerAdapter);
        }

        return mParentView;
    }

    @OnTextChanged(R.id.keyword_input)
    protected void onTextChanged(CharSequence text) {
        String featureName = text.toString();
        onTextChange();
    }

    public void onTextChange() {
        mDataQuery.clear();
        String text = mKeyword.getText().toString().toLowerCase();
        for(String dataCopy: mDataCopy) {
            if(dataCopy.toLowerCase().contains(text)) {
                mDataQuery.add(dataCopy);
            }
        }
        mDataChange.clear();
        mDataChange.addAll(mData);
        mDataChange.removeAll(mDataQuery);
        for(String del: mDataChange){
            for(int i = 0; i < mData.size(); i++) {
                if(mData.get(i).equals(del)) {
                    mData.remove(i);
                    mContactsRecyclerAdapter.notifyItemRemoved(i);
                }
            }
        }
        mDataChange.clear();
        mDataChange.addAll(mDataQuery);
        mDataChange.removeAll(mData);
        for(String insert: mDataChange){
            boolean isInsert = false;
            for(int i = 0; i < mData.size(); i++) {
                if(mData.get(i).compareTo(insert) >= 0) {
                    mData.add(i, insert);
                    mContactsRecyclerAdapter.notifyItemInserted(i);
                    isInsert = true;
                    break;
                }
            }
            if(!isInsert) {
                mData.add(mData.size(), insert);
                mContactsRecyclerAdapter.notifyItemInserted(mData.size());
            }
        }

    }
    public void onFindKeyword(String number) {
        mKeyword.setText(number);
    }

    @Override
    public void onMenuItemClicked(String name, String number) {
        if(phoneInputFragment != null) {
            phoneInputFragment.onCall(name, number);
        }
    }

    @Override
    public void onMenuItemLongClicked(final int position, final String name, final String number) {
        BottomSheet.Builder builder = new BottomSheet.Builder(getContext());
        builder.setTitleColor(getResources().getColor(R.color.colorPrimary));
        builder.setTitle(name);
        builder.addItem(0, R.string.edit_call, R.drawable.ic_phone_black);
        builder.addItem(1, R.string.edit_contacts, R.drawable.ic_edit_black);
        builder.addItem(2, R.string.delete_contacts, R.drawable.ic_delete_black);
        builder.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int p, long id) {
                switch ((int) id) {
                    case 0:
                        if(phoneInputFragment != null) {
                            phoneInputFragment.onSetToBoard(name, number);
                        }
                        break;
                    case 1:
                        ((MainActivity) getActivity()).onUpdateContacts(name, number);
                        break;
                    case 2:
                        onDeleteItem(position);
                        break;
                }
            }
        });
        BottomSheet bottomSheet = builder.create();
        bottomSheet.show();
    }

    @Override
    public void onCallClicked(String name, String number) {
        if(phoneInputFragment != null) {
            phoneInputFragment.onCall(name, number);
        }
    }

    @Override
    public void onMessageClicked(String name, String number) {
        ((MainActivity)getContext()).onNewMessage(name, number);
    }

    public void onDeleteItem(int position) {
        String del = mData.remove(position);
        mDataCopy.remove(del);

        mContactsRecyclerAdapter.notifyItemRemoved(position);
    }
}
