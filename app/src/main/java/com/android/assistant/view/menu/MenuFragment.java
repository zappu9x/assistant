package com.android.assistant.view.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.assistant.R;
import com.android.assistant.utils.Const;
import com.android.assistant.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.assistant.utils.Common.goBack;
import static com.android.assistant.utils.Common.openVoice;
import static com.android.assistant.utils.Common.switchToScreen;

public class MenuFragment extends Fragment implements MenuRecyclerAdapter.OnItemClickCallBack {
    public static String WELLCOME = "Say your function you want use!. Example!!! Say phone to get phone. ...etc..";
    private View mParentView;
    @BindView(R.id.recycle_view)
    RecyclerView mRecyclerView;


    public static MenuFragment newInstance() {
        MenuFragment menuFragment = new MenuFragment();
        return menuFragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //openVoice(getContext(), WELLCOME, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_menu, container, false);
            ButterKnife.bind(this, mParentView);
            mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
            mRecyclerView.setAdapter(new MenuRecyclerAdapter(getContext(), this));
        }
        return mParentView;
    }


    public void onRecognize(String text) {
        boolean isSuccess = false;
        for(String tag: Const.MenuCommands) {
            if(text.equals(tag)) {
                switchToScreen(getContext(), tag);
                isSuccess = true;
                break;
            }
        }
        if(!isSuccess) {
            if(text.equals("back")){
                openVoice(getContext(), "OK. Go back!", true);
                goBack(getContext());
            } else if(text.equals("back to home")) {
                openVoice(getContext(), "OK. Go back to home!", true);
                goBack(getContext());
            } else if(text.equals("stop")) {
                openVoice(getContext(), "OK. Bye. See you again!", false);
            }
            else {
                openVoice(getContext(), "Sorry. I dont understand!", true);
            }
        }
    }

    @Override
    public void onMenuItemClicked(String tag) {
        ((MainActivity)getContext()).switchFragmentByTag(tag, false);
    }
}
