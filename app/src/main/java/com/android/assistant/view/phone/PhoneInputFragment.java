package com.android.assistant.view.phone;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.assistant.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneInputFragment extends Fragment {
    private View mParentView;
    private PhonePagerAdapter mPagerAdapter;
    private OnCallBack onCallBack;
    @BindView(R.id.pager)ViewPager mPager;
    @BindView(R.id.tab_layout) TabLayout mTabLayout;

    public static PhoneInputFragment newInstance(OnCallBack onCallBack) {
        PhoneInputFragment phoneInputFragment = new PhoneInputFragment();
        phoneInputFragment.onCallBack = onCallBack;
        return phoneInputFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_phone_input, container, false);
            ButterKnife.bind(this, mParentView);
            mPagerAdapter = new PhonePagerAdapter(getChildFragmentManager(), this);
            mPager.setAdapter(mPagerAdapter);
            mTabLayout.setupWithViewPager(mPager);
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_history);
            mTabLayout.getTabAt(0).getIcon().setColorFilter(getContext().getResources().getColor(R.color.un_selected), PorterDuff.Mode.SRC_IN);
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_dialpad);
            mTabLayout.getTabAt(1).getIcon().setColorFilter(getContext().getResources().getColor(R.color.un_selected), PorterDuff.Mode.SRC_IN);
            mTabLayout.getTabAt(2).setIcon(R.drawable.ic_contact);
            mTabLayout.getTabAt(2).getIcon().setColorFilter(getContext().getResources().getColor(R.color.un_selected), PorterDuff.Mode.SRC_IN);
            mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    tab.getIcon().setColorFilter(getContext().getResources().getColor(R.color.selected), PorterDuff.Mode.SRC_IN);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    tab.getIcon().setColorFilter(getContext().getResources().getColor(R.color.un_selected), PorterDuff.Mode.SRC_IN);
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    tab.getIcon().setColorFilter(getContext().getResources().getColor(R.color.selected), PorterDuff.Mode.SRC_IN);

                }
            });
            mPager.setCurrentItem(1);
        }

        return mParentView;
    }


    public void onSetToBoard(String name, String number) {
        mPager.setCurrentItem(1);
        mPagerAdapter.onSetToBoard(name, number);
    }

    public void onFindInContacts(String number) {
        mPager.setCurrentItem(2);
        mPagerAdapter.onFindInContacts(number);
    }

    public void onCall(String name, String number) {
        if(onCallBack!=null) {
            onCallBack.onGoCallScreen(name, number);
        }
    }

    public interface OnCallBack {
        void onGoCallScreen(String name, String number);
    }
}
