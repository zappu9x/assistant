package com.android.assistant.view.phone;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.assistant.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryRecyclerAdapter extends RecyclerView.Adapter<HistoryRecyclerAdapter.ViewHolder>{

    private ArrayList<String> historyModels;
    private Context context;

    public HistoryRecyclerAdapter(Context context, ArrayList<String> historyModels, HistoryRecyclerAdapter.OnItemClickCallBack onItemClickCallBack) {
        this.context = context;
        this.historyModels = historyModels;
        this.onItemClickCallBack = onItemClickCallBack;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_history, parent, false);
        ViewHolder viewHolder = new ViewHolder(convertView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String object[] = historyModels.get(position).split(":");
        holder.name.setText(object[0].trim());
        if(object.length > 1) {
            holder.icon.setImageResource(R.drawable.ic_account);
            //holder.number.setText(object[1].trim());
            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickCallBack != null) onItemClickCallBack.onMenuItemClicked(object[0].trim(), object[1].trim());
                }
            });
            holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(onItemClickCallBack != null) onItemClickCallBack.onMenuItemLongClicked(holder.getAdapterPosition(), object[0].trim(), object[1].trim());
                    return true;
                }
            });
            holder.message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickCallBack != null) onItemClickCallBack.onMessageClicked(object[0].trim(), object[1].trim());
                }
            });
        } else {
            holder.icon.setImageResource(R.drawable.ic_phone_call);
            //holder.number.setText("");
            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickCallBack != null) onItemClickCallBack.onMenuItemClicked("", object[0].trim());
                }
            });
            holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(onItemClickCallBack != null) onItemClickCallBack.onMenuItemLongClicked(holder.getAdapterPosition(), "", object[0].trim());
                    return true;
                }
            });
            holder.message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickCallBack != null) onItemClickCallBack.onMessageClicked("", object[0].trim());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return historyModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.name) TextView name;
        @BindView(R.id.icon) ImageView icon;
        @BindView(R.id.number)TextView number;
        @BindView(R.id.message)ImageButton message;
        @BindView(R.id.layout) LinearLayout layout;

        public ViewHolder(View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);
        }
    }

    public OnItemClickCallBack onItemClickCallBack;
    public interface OnItemClickCallBack{
        void onMenuItemClicked(String name, String number);
        void onMessageClicked(String name, String number);
        void onMenuItemLongClicked(int position, String name, String number);
    }
}