package com.android.assistant.view.message;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.assistant.R;
import com.android.assistant.utils.Const;
import com.android.assistant.view.MainActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

import static com.android.assistant.utils.Common.goBack;
import static com.android.assistant.utils.Common.openMic;
import static com.android.assistant.utils.Common.openVoice;
import static com.android.assistant.utils.Common.switchToScreen;

public class MesageFragment extends Fragment  {


    public static String WELLCOME = "Say  " +
            "New message. or. Open message";

    private View mParentView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //openVoice(getContext(), WELLCOME, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_message, container, false);
            ButterKnife.bind(this, mParentView);
        }

        return mParentView;
    }

    public static MesageFragment newInstance() {
        MesageFragment updateContactsFragment = new MesageFragment();
        return updateContactsFragment;
    }

    @OnClick(R.id.open_button)
    public void onOpenClick(View v) {
    }

    @OnClick(R.id.send_button)
    public void onSendClick(View v) {
        ((MainActivity)getContext()).switchFragmentByTag(Const.NEW_MESSAGE_TAG, false);
    }

    @OnLongClick(R.id.send_button)
    public boolean onLongClick(View v) {
        openMic(getContext());
        return true;
    }

    public void onRecognize(String text) {
        if (text.equals("back")) {
            openVoice(getContext(), "OK. Go back!", true);
            goBack(getContext());
        } else if (text.equals("back to home")) {
            openVoice(getContext(), "OK. Go back to home!", true);
            goBack(getContext());
        } else if (text.equals("stop")) {
            openVoice(getContext(), "OK. Bye. See you again!", false);
        } else if (text.contains("new message")) {
            switchToScreen(getContext(), Const.NEW_MESSAGE_TAG);
        } else if (text.contains("open message")) {
            openVoice(getContext(), "Sorry, this function havent design yet!", true);
        } else {
            openVoice(getContext(), "Sorry. I dont understand!", true);
        }
    }
}
