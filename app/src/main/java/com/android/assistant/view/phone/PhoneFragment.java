package com.android.assistant.view.phone;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.assistant.R;
import com.android.assistant.utils.Const;
import com.android.assistant.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.assistant.R.id.number;
import static com.android.assistant.utils.Common.getContacts;
import static com.android.assistant.utils.Common.getMainActivity;
import static com.android.assistant.utils.Common.goBack;
import static com.android.assistant.utils.Common.openMic;
import static com.android.assistant.utils.Common.openVoice;
import static com.android.assistant.utils.Common.tryToConvertNumber;

public class PhoneFragment extends Fragment {

    public static String WELLCOME = "Say  " +
            "Number one two three. if want call one two three. Or. " +
            "Name hello. To call hello.";

    private View mParentView;
    @BindView(R.id.name)
    TextView nameView;
    @BindView(number)
    TextView numberView;


    public static PhoneFragment newInstance() {
        PhoneFragment fragment = new PhoneFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //openVoice(getContext(), WELLCOME, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_phone, container, false);
            ButterKnife.bind(this, mParentView);
        }
        return mParentView;
    }

    @OnClick(R.id.board_button)
    public void onGoPhoneScreen(View v){
        ((MainActivity)getContext()).switchFragmentByTag(Const.PHONE_INPUT_TAG, false);
    }
    @OnClick(R.id.mic_button)
    public void onShowAsistant(View v){
        openMic(getContext());
    }

    public void setData(String name, String number) {
        nameView.setText(name);
        numberView.setText(number);
    }
    public void setDataByContacts(String s) {
        String name = s.split(":")[0].trim();
        String number = s.split(":")[1].trim();
        setData(name, number);
    }


    public void onRecognize(String text) {
        if(text.equals("back")){
            openVoice(getContext(), "OK. Go back!", true);
            goBack(getContext());
        } else if(text.equals("back to home")) {
            openVoice(getContext(), "OK. Go back to home!", true);
            goBack(getContext());
        } else if(text.equals("stop")) {
            openVoice(getContext(), "OK. Bye. See you again!", false);
        }
        else {
            String[] array = text.split("\\s+");
            if (text.contains("call") && (!nameView.getText().toString().isEmpty() || !numberView.getText().toString().isEmpty())) {
                getMainActivity(getContext()).onGoCallScreen(nameView.getText().toString().trim(), numberView.getText().toString().trim());
                openVoice(getContext(), "OK, Connecting to !" + nameView.getText().toString() + "." + CallFragment.WELLCOME, false);
            } else {
                if (text.contains("number")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("number")) {
                            isAdd = true;
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty() && !tryToConvertNumber(t).isEmpty()) {
                        openVoice(getContext(), " Do you want call to number. " + tryToConvertNumber(t), true);
                        setData(tryToConvertNumber(t), "");
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the number you say!", true);
                    }
                } else if (text.contains("name")) {
                    String t = "";
                    boolean isAdd = false;
                    for (String s : array) {
                        if (s.equals("name")) {
                            isAdd = true;
                            t = "";
                        } else if (isAdd) {
                            t += s + " ";
                        }
                    }
                    if (!t.isEmpty()) {
                        boolean isSuccess = false;
                        for (String contacts : getContacts()) {
                            if (contacts.contains(t.trim())) {
                                openVoice(getContext(), " Do you want call to " + contacts, true);
                                setDataByContacts(contacts);
                                isSuccess = true;
                                break;
                            }
                        }
                        if (!isSuccess) {
                            openVoice(getContext(), " The name " + t + " not exit ", true);
                        }
                    } else {
                        openVoice(getContext(), " Sorry!. cant understant the name you say!", true);
                    }
                } else {
                    openVoice(getContext(), "Sorry. I dont understand!", true);
                }
            }
        }
    }


    public interface OnCallBack {
        void onGoPhoneScreen();
    }
}
