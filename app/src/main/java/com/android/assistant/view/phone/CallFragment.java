package com.android.assistant.view.phone;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.assistant.R;
import com.android.assistant.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

import static com.android.assistant.utils.Common.openVoice;

public class CallFragment extends Fragment {
    private View mParentView;
    private String name, number;
    @BindView(R.id.number)
    TextView mNumberView;
    @BindView(R.id.name)
    TextView mNameView;
    @BindView(R.id.status)
    TextView mStatusView;
    @BindView(R.id.volume_button)
    ImageButton mVolumeButton;
    @BindView(R.id.mic_button)
    ImageButton mMicButton;
    private boolean isCloseMic;
    private boolean isCloseVolume;


    public static String WELLCOME = "If you want to disconnect the call say. disconnect ....etc..";

    public static CallFragment newInstance(String name, String number) {
        CallFragment callFragment = new CallFragment();
        callFragment.name = name;
        callFragment.number = number;
        return callFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(mParentView == null) {
            mParentView = inflater.inflate(R.layout.fragment_call, container, false);
            ButterKnife.bind(this, mParentView);
            if(name.isEmpty()){
                mNumberView.setVisibility(View.GONE);
                mNameView.setText(number);
            }
            else {
                mNumberView.setVisibility(View.VISIBLE);
                mNameView.setText(name);
                mNumberView.setText(number);
            }
            mStatusView.setText("Connecting...");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mStatusView.setText("Renging...");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mStatusView.setText("00:00");
                        }
                    }, 3000);
                }
            }, 3000);
        }
        return mParentView;
    }

    @OnClick(R.id.mic_button)
    public void onSwitchMic(View v){
        isCloseMic = !isCloseMic;
        mMicButton.setImageResource(isCloseMic ? R.drawable.ic_microphone_off : R.drawable.ic_microphone);
        Toast.makeText(getContext(), "Turn " + (isCloseMic ? "off" : "on") + " micro!", Toast.LENGTH_SHORT).show();

    }
    @OnClick(R.id.volume_button)
    public void onSwitchVolume(View v){
        isCloseVolume = !isCloseVolume;
        mVolumeButton.setImageResource(isCloseVolume ? R.drawable.ic_volume_off : R.drawable.ic_volume_on);
        Toast.makeText(getContext(), "Turn " + (isCloseMic ? "off" : "on") + " volume!", Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.end_call_button)
    public void onEndCall(View v) {
        onEndCall();
    }

    @OnLongClick(R.id.end_call_button)
    public boolean onEndCallUsingVoice(View v) {
        ((MainActivity) getActivity()).startSpeechToText();
        return true;
    }

    public void onEndCall(){
        mStatusView.setText("Ending...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getActivity().onBackPressed();
            }
        }, 1000);
    }

    public void onRecognize(String text) {
        if (text.equals("back") || text.equals("disconnect")) {
            openVoice(getContext(), "OK. disconecting the call!. And go back to previous!", true);
            onEndCall();
        } else if (text.equals("back to home")) {
            openVoice(getContext(), "OK.  disconecting the call!. Go back to home!", true);
            onEndCall();
        } else if (text.equals("stop")) {
            openVoice(getContext(), "OK. Bye. See you again!", false);
        } else {
            openVoice(getContext(), "Sorry. I dont understand!", true);
        }

    }
}
