package com.android.assistant.view.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.assistant.R;
import com.android.assistant.utils.Const;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuRecyclerAdapter extends RecyclerView.Adapter<MenuRecyclerAdapter.ViewHolder>{

    private Context context;
    private int drawableColorID[];
    private int drawableIconID[];
    private String drawableTite[];

    public MenuRecyclerAdapter(Context context, OnItemClickCallBack onItemClickCallBack) {
        this.context = context;
        this.onItemClickCallBack = onItemClickCallBack;
        drawableColorID = new int[]{
                R.drawable.c1_bg,
                R.drawable.c2_bg,
                R.drawable.c3_bg,
                R.drawable.c4_bg,
                R.drawable.c5_bg,
                R.drawable.c6_bg,
                R.drawable.c7_bg,
                R.drawable.c8_bg,
                R.drawable.c9_bg,
                R.drawable.c10_bg};

        drawableIconID = new int[]{
                R.drawable.ic_phone_call,
                R.drawable.ic_contact,
                R.drawable.ic_message,
                R.drawable.ic_facebook,
                R.drawable.ic_camera,
                R.drawable.ic_radio,
                R.drawable.ic_alarm,
                R.drawable.ic_game,
                R.drawable.ic_maps,
                R.drawable.ic_settings
        };

        drawableTite = new String[]{
                "Phone",
                "Contacts",
                "Message",
                "Facebook",
                "Camera",
                "Radio",
                "Alarm",
                "Play game",
                "Google maps",
                "Setting"
        };
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_menu, parent, false);
        ViewHolder viewHolder = new ViewHolder(convertView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textView.setText(drawableTite[position]);
        holder.imageView.setImageResource(drawableIconID[position]);
        holder.imageView.setBackgroundResource(drawableColorID[position]);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickCallBack != null) {
                    onItemClickCallBack.onMenuItemClicked(Const.TAG_LIST[position]);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return drawableTite.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.textview)
        TextView textView;
        @BindView(R.id.imageview)
        ImageView imageView;
        public ViewHolder(View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);
        }
    }
    public OnItemClickCallBack onItemClickCallBack;
    public interface OnItemClickCallBack{
        void onMenuItemClicked(String tag);
    }
}