package com.android.assistant.view;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.assistant.R;
import com.android.assistant.utils.Const;
import com.android.assistant.view.contact.ContactsFragment;
import com.android.assistant.view.contact.UpdateContactsFragment;
import com.android.assistant.view.menu.MenuFragment;
import com.android.assistant.view.message.MesageFragment;
import com.android.assistant.view.message.NewMesageFragment;
import com.android.assistant.view.phone.CallFragment;
import com.android.assistant.view.phone.CommingFragment;
import com.android.assistant.view.phone.PhoneFragment;
import com.android.assistant.view.phone.PhoneInputFragment;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements
        PhoneInputFragment.OnCallBack {
    private TextToSpeech mTextToSpeech;
    private Intent mSpeechToTextIntent;
    private String mWelComeText = "Hi, how can i help!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        switchFragmentByTag(Const.MAIN_TAG, false);
        initSpeechToText();
        initTextToSpeech();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startTextToSpeech(mWelComeText, false);
            }
        }, 500);
    }

    private void initTextToSpeech() {
        mTextToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTextToSpeech.setLanguage(Locale.ENGLISH);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(getApplicationContext(), "This text to speech not supported", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    private void initSpeechToText() {
        mSpeechToTextIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechToTextIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechToTextIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en");
        mSpeechToTextIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "What do you want?\nexample: \"menu\"");

    }
    public void startSpeechToText() {
        try {
            startActivityForResult(mSpeechToTextIntent, Const.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),"This speech to text not supported", Toast.LENGTH_SHORT).show();
        }
    }
    public void startTextToSpeech(String text, final boolean isShowAgain) {
        if (mTextToSpeech != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, Const.MAIN_TAG);
            } else {
                mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                mTextToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceId) {

                    }

                    @Override
                    public void onDone(String utteranceId) {
                        Log.d("ddddd", utteranceId);
                        if(isShowAgain) startSpeechToText();
                    }

                    @Override
                    public void onError(String utteranceId) {

                    }
                });
            }
        }
    }

    public void switchFragmentByTag(String tag, boolean isVoice) {
        Fragment fragment = null;
        if(isVoice) {
            switch (tag) {
                case Const.MENU_TAG:
                    fragment = MenuFragment.newInstance();
                    startTextToSpeech(MenuFragment.WELLCOME, true);
                    break;
                case Const.MAIN_TAG:
                    fragment = MainFragment.newInstance();
                    startTextToSpeech(MainFragment.WELLCOME, true);
                    break;
                case Const.PHONE_INPUT_TAG:
                    fragment = PhoneInputFragment.newInstance(this);
                    break;
                case Const.PHONE_TAG:
                    fragment = PhoneFragment.newInstance();
                    startTextToSpeech(PhoneFragment.WELLCOME, true);
                    break;
                case Const.CONTACTS_TAG:
                    fragment = new ContactsFragment();
                    startTextToSpeech(ContactsFragment.WELLCOME, true);
                    break;
                case Const.UPDATE_CONTACTS_TAG:
                    fragment = new UpdateContactsFragment();
                    startTextToSpeech(UpdateContactsFragment.WELLCOME, true);
                    break;
                case Const.MESAGE_TAG:
                    fragment = new MesageFragment();
                    startTextToSpeech(MesageFragment.WELLCOME, true);
                    break;
                case Const.NEW_MESSAGE_TAG:
                    fragment = new NewMesageFragment();
                    startTextToSpeech(NewMesageFragment.WELLCOME, true);
                    break;
                case Const.COMMING_TAG:
                    fragment = new CommingFragment();
                    startTextToSpeech(CommingFragment.WELLCOME, true);
                    break;
            }
        } else {
            switch (tag) {
                case Const.MENU_TAG:
                    fragment = MenuFragment.newInstance();
                    break;
                case Const.MAIN_TAG:
                    fragment = MainFragment.newInstance();
                    break;
                case Const.PHONE_INPUT_TAG:
                    fragment = PhoneInputFragment.newInstance(this);
                    break;
                case Const.PHONE_TAG:
                    fragment = PhoneFragment.newInstance();
                    break;
                case Const.CONTACTS_TAG:
                    fragment = new ContactsFragment();
                    break;
                case Const.UPDATE_CONTACTS_TAG:
                    fragment = new UpdateContactsFragment();
                    break;
                case Const.NEW_MESSAGE_TAG:
                    fragment = new NewMesageFragment();
                    break;
                case Const.MESAGE_TAG:
                    fragment = new MesageFragment();
                    break;
                case Const.COMMING_TAG:
                    fragment = new CommingFragment();
                    break;
            }
        }
        if (fragment != null) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
            transaction.replace(R.id.frame_layout, fragment)
                    .addToBackStack(tag)
                    .commit();
        }
    }


    @Override
    protected void onPause() {
        if(mTextToSpeech !=null){
            mTextToSpeech.stop();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(mTextToSpeech !=null){
            mTextToSpeech.stop();
            mTextToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Const.REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    recognize(result.get(0));
                }
                break;
            }

        }
    }

    public void recognize(String text) {
        text = text.toLowerCase().trim();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
        Log.d("aaaaa", getActiveFragmentTag());
        switch (getActiveFragmentTag()){
            case Const.MAIN_TAG:
                ((MainFragment) fragment).onRecognize(text);
                break;
            case Const.MENU_TAG:
                ((MenuFragment) fragment).onRecognize(text);
                break;
            case Const.PHONE_TAG:
                ((PhoneFragment) fragment).onRecognize(text);
                break;
            case Const.CALL_TAG:
                ((CallFragment) fragment).onRecognize(text);
                break;
            case Const.UPDATE_CONTACTS_TAG:
                ((UpdateContactsFragment) fragment).onRecognize(text);
                break;
            case Const.CONTACTS_TAG:
                ((ContactsFragment) fragment).onRecognize(text);
                break;
            case Const.NEW_MESSAGE_TAG:
                ((NewMesageFragment) fragment).onRecognize(text);
                break;
            case Const.MESAGE_TAG:
                ((MesageFragment) fragment).onRecognize(text);
                break;
            case Const.COMMING_TAG:
                ((CommingFragment) fragment).onRecognize(text);
                break;
        }
    }

    public void onUpdateContacts(String name, String number) {
        Fragment fragment = UpdateContactsFragment.newInstance(name, number);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.replace(R.id.frame_layout, fragment)
                .addToBackStack(Const.UPDATE_CONTACTS_TAG)
                .commit();
    }

    public void onNewMessage(String name, String number) {
        Fragment fragment = NewMesageFragment.newInstance(name, number);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.replace(R.id.frame_layout, fragment)
                .addToBackStack(Const.NEW_MESSAGE_TAG)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



    @Override
    public void onGoCallScreen(String name, String number) {
        Fragment fragment = CallFragment.newInstance(name, number);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.replace(R.id.frame_layout, fragment)
                .addToBackStack(Const.CALL_TAG)
                .commit();
    }

    public String getActiveFragmentTag() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }
        String tag = getSupportFragmentManager().getBackStackEntryAt(
                getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return tag;
    }
}
